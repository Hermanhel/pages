:PROPERTIES:
:ID:       ac50d79c-ef61-4e03-a323-38f98bf8fef7
:DIR:      ~/playground/projects/codeberg-pages/
:END:
#+title: Gehninfel
#+SETUPFILE: ./white_clean.theme

#+begin_quote
......One month every year, the door to Gehninfel is open through the portal underneath the nevermelting ice shell of South Pole......
#+end_quote

#+caption: map of gehninfel and earth
#+attr_html: :width 600
[[./_20240105_055945screenshot.png]]


Portal to placesl
+ [[https://z.gehninfel.xyz][zettelkasten]]
+ [[https://z.gehninfel.xyz/main/about.html][about]]
  #+begin_export html
<center>
<a href="https://webring.xxiivv.com/#gehninfel" target="_blank" rel="noopener">
  <img src="https://webring.xxiivv.com/icon.black.svg" alt="XXIIVV webring"/>
</a>
<p><span class="figure-number">Webring</span></p>
</center>
  #+end_export
